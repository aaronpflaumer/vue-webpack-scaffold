# Vue-Webpack-Scaffold
Scaffolding for creating a project using Vue.js and Webpack.

#### Technologies
* [Webpack](https://webpack.js.org/)
* [Vue.js](https://vuejs.org/)
* [Vuex](https://vuex.vuejs.org/en/)
* [Onsen UI](https://onsen.io/)
* [Cordova](https://cordova.apache.org/)
