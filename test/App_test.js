import Vue from 'vue'
import App from '../src/js/components/App.vue'

describe('App Component', () => {
  it('should have imported correctly', () => {
    expect(App).toBeDefined()
  })

  it('has a created hook', () => {
    expect(typeof App.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof App.data).toBe('function')
    const defaultData = App.data()
    expect(defaultData.msg).toBe('Welcome to Your Vue.js App')
  })

  it('correctly sets the message when created', () => {
    const vm = new Vue(App).$mount()
    expect(vm.message).toBe('I exist!')
  })

  it('renders the correct message', () => {
    const Ctor = Vue.extend(App)
    const vm = new Ctor().$mount()
    expect(vm.$el.textContent).toBe('I exist!')
  })
});
